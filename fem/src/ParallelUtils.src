!/*****************************************************************************/
! *
! *  Elmer, A Finite Element Software for Multiphysical Problems
! *
! *  Copyright 1st April 1995 - , CSC - IT Center for Science Ltd., Finland
! * 
! * This library is free software; you can redistribute it and/or
! * modify it under the terms of the GNU Lesser General Public
! * License as published by the Free Software Foundation; either
! * version 2.1 of the License, or (at your option) any later version.
! *
! * This library is distributed in the hope that it will be useful,
! * but WITHOUT ANY WARRANTY; without even the implied warranty of
! * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
! * Lesser General Public License for more details.
! * 
! * You should have received a copy of the GNU Lesser General Public
! * License along with this library (in file ../LGPL-2.1); if not, write 
! * to the Free Software Foundation, Inc., 51 Franklin Street, 
! * Fifth Floor, Boston, MA  02110-1301  USA
! *
! *****************************************************************************/
!
!/******************************************************************************
! *
! *  Authors: Juha Ruokolainen
! *  Email:   Juha.Ruokolainen@csc.fi
! *  Web:     http://www.csc.fi/elmer
! *  Address: CSC - IT Center for Science Ltd.
! *           Keilaranta 14
! *           02101 Espoo, Finland 
! *
! *  Original Date: 02 Apr 2001
! *
! *****************************************************************************/

!> \ingroup ElmerLib
!> \{

!------------------------------------------------------------------------------
!>  Parallel solver utilities for *Solver - routines
!------------------------------------------------------------------------------

MODULE ParallelUtils

     USE SparIterSolve

     IMPLICIT NONE

CONTAINS

#define PARALLEL_FOR_REAL
!-------------------------------------------------------------------------------
  FUNCTION ParallelInit() RESULT ( ParallelEnv )
!-------------------------------------------------------------------------------
    TYPE (ParEnv_t), POINTER :: ParallelEnv
                                                                                                                               
#ifdef PARALLEL_FOR_REAL
    ParallelEnv => ParCommInit( )
#else
    ParEnv % MyPE = 0
    ParEnv % PEs  = 1
    ParallelEnv => ParEnv
#endif
!-------------------------------------------------------------------------------
  END FUNCTION ParallelInit
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
  SUBROUTINE ParallelFinalize()
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
    CALL ParEnvFinalize()
#endif
!-------------------------------------------------------------------------------
  END SUBROUTINE ParallelFinalize
!-------------------------------------------------------------------------------
                                                                                                                               
!--------------------------------'-----------------------------------------------
    SUBROUTINE ParallelInitMatrix( Solver, Matrix )
!-------------------------------------------------------------------------------
       TYPE(Solver_t) :: Solver
       TYPE(Matrix_t), POINTER :: Matrix
!-------------------------------------------------------------------------------
       TYPE(ParallelInfo_t), POINTER :: MatrixPI, MeshPI
       INTEGER :: i, j, k, l, m, n, DOFs
       LOGICAL :: DGSolver, Found, GB
       TYPE(Mesh_t), POINTER :: Mesh
       TYPE(Element_t), POINTER :: Element
       TYPE(NeighbourList_t), POINTER :: MtrxN, MeshN

       INTEGER :: maxnode, maxedge, maxface, fdofs, maxfdofs, &
                  edofs, maxedofs, maxbdofs, l_beg, g_beg

       REAL(KIND=dp) :: realtime,tt
!-------------------------------------------------------------------------------

!tt = realtime()
#ifdef PARALLEL_FOR_REAL
       IF ( ParEnv % PEs <= 1 .OR. .NOT. ASSOCIATED(Solver % Matrix) ) RETURN
       Mesh => Solver % Mesh
       DOFs = Solver % Variable % DOFs
       n = SIZE(Solver % Variable % Perm)

       ALLOCATE( Matrix % Perm(DOFs*n), Matrix % InvPerm(DOFs*n)  )
       Matrix % Perm    = 0
       Matrix % INVPerm = 0

       DO i=1,n
          IF ( Solver % Variable % Perm(i) /= 0 )  THEN
             DO j=1,DOFs
                Matrix % Perm( (i-1)*DOFs+j ) = &
                   DOFs * (Solver % Variable % Perm(i)-1) + j
             END DO
          END IF
       END DO

       DO i=1,DOFs*n
          IF ( Matrix % Perm(i) /= 0 ) THEN
             Matrix % INVPerm(Matrix % Perm(i)) = i
          END IF
       END DO

       IF ( .NOT. Matrix % DGMatrix ) THEN
         n = Matrix % NumberOfRows
         ALLOCATE( Matrix % ParallelInfo )
         ALLOCATE( Matrix % ParallelInfo % NeighbourList(n) )
         CALL AllocateVector( Matrix % ParallelInfo % Interface, n)
         CALL AllocateVector( Matrix % ParallelInfo % GlobalDOFs, n)

         DO i=1,n
           Matrix % ParallelInfo % Interface(i) = .FALSE.
           Matrix % ParallelInfo % GlobalDOFs(i) = 0
           Matrix % ParallelInfo % NeighbourList(i) % Neighbours => NULL()
         END DO

         DO i=1,Solver % Mesh % NumberOfNodes
           DO j=1,DOFs
              k = Matrix % Perm((i-1)*DOFs+j)
              IF(k==0) CYCLE
              Matrix % ParallelInfo % GlobalDOFs(k) = &
                DOFs*(Solver % Mesh % ParallelInfo % GlobalDOFs(i)-1)+j
              Matrix % ParallelInfo % Interface(k) = &
                Solver % Mesh % ParallelInfo % Interface(i)
              ALLOCATE( Matrix % ParallelInfo % NeighbourList(k) % Neighbours(SIZE( &
                   Solver % Mesh % ParallelInfo % NeighbourList(i) % Neighbours)) )
              Matrix % ParallelInfo % NeighbourList(k) % Neighbours = &
                Solver % Mesh % ParallelInfo % NeighbourList(i) % Neighbours
           END DO
         END DO

         GB = ListGetLogical( Solver % Values, 'Bubbles in Global System', Found )
         IF (.NOT.Found) GB = .TRUE.

         maxnode = MAXVAL(Solver % Mesh % ParallelInfo % GlobalDOFs)
         maxnode = NINT(ParallelReduction(1._dp*maxnode,2))

         edofs = 0; fdofs = 0; maxedofs = 0; maxfdofs = 0
         maxedge = 0; maxface = 0

         IF ( ASSOCIATED(Solver % Mesh % Edges) ) THEN
           g_beg = maxnode
           l_beg = Solver % Mesh % NumberOfNodes

           n = Solver % Mesh % NumberOfEdges

           edofs = Solver % Mesh % MaxEdgeDOFS
           maxedofs = NINT(ParallelReduction(edofs*1._dp,2))

           maxedge = 0
           DO i=1,n
             maxedge = MAX(maxedge, Solver % Mesh % Edges(i) % GElementindex)
           END DO
           maxedge = NINT(ParallelReduction(1._dp*maxedge,2))

           DO i=1,n
             Element => Solver % Mesh % Edges(i)
             DO j=1,Element % BDOFs
               DO m=1,DOFs
                 l = Matrix % Perm(DOFs*(l_beg + edofs*(i-1)+j-1)+m)
                 IF(l==0) CYCLE
                 Matrix % ParallelInfo % GlobalDOFs(l) = &
                     DOFs*(g_beg+maxedofs*(Element % GelementIndex-1)+j-1)+m
                 Matrix % ParallelInfo % Interface(l) = Mesh % ParallelInfo % EdgeInterface(i)
                 Matrix % Parallelinfo % NeighbourList(l) % Neighbours => &
                            Mesh % ParallelInfo % EdgeNeighbourList(i) % Neighbours
               END DO
             END DO
           END DO
         END IF

         IF ( ASSOCIATED(Solver % Mesh % Faces) ) THEN
           g_beg = maxnode + maxedofs*maxedge
           l_beg = Solver % Mesh % NumberOfNodes + &
                   Solver % Mesh % NumberOfEdges*Solver % Mesh % MaxEdgeDOFs

           n = Solver % Mesh % NumberOfFaces

           fdofs = Solver % Mesh % MaxFaceDOFS
           maxfdofs = NINT(ParallelReduction(fdofs*1._dp,2))

           maxface = 0
           DO i=1,n
             maxface = MAX(maxface, Solver % Mesh % Faces(i) % GElementindex)
           END DO
           maxface = NINT(ParallelReduction(1._dp*maxface,2))

           DO i=1,n
             Element => Solver % Mesh % Faces(i)
             DO j=1,Element % BDOFs
               DO m=1,DOFs
                 l = Matrix % Perm(DOFs*(l_beg + fdofs*(i-1)+j-1)+m)
                 IF(l==0) CYCLE
                 Matrix % ParallelInfo % GlobalDOFs(l) = &
                     DOFs*(g_beg+maxfdofs*(Element % GelementIndex-1)+j-1)+m
                 Matrix % ParallelInfo % Interface(l) = Mesh % ParallelInfo % FaceInterface(i)
                 Matrix % Parallelinfo % NeighbourList(l) % Neighbours => &
                            Mesh % ParallelInfo % FaceNeighbourList(i) % Neighbours
               END DO
             END DO
           END DO
         END IF

         IF ( GB ) THEN
           l_beg = Solver % Mesh % NumberOfNodes + &
                   Solver % Mesh % NumberOfEdges*Solver % Mesh % MaxEdgeDOFs + &
                   Solver % Mesh % NumberOfFaces*Solver % Mesh % MaxFaceDOFs

           g_beg = Maxnode +  maxedge*maxedofs + maxface*maxfdofs
           maxbdofs = NINT(ParallelReduction(1._dp*Solver % Mesh % MaxBDOFs,2))

           DO i=1,Solver % Mesh % NumberOfBulkElements
             Element=>Solver % Mesh % Elements(i)
             DO l=1,Element % BDOFs
               DO j=1,DOFs 
                 k = Matrix % Perm(DOFs*(l_beg+Element % BubbleIndexes(l)-1)+j)
                 IF(k==0) CYCLE
                 Matrix % ParallelInfo % GlobalDOFs(k) = &
                   DOFs*(g_beg+maxbdofs*(Element % GElementIndex-1)+l-1)+j
                 Matrix % ParallelInfo % Interface(k) = .FALSE.
                 ALLOCATE(Matrix % ParallelInfo % NeighbourList(k) % Neighbours(1))
                 Matrix % ParallelInfo % NeighbourList(k) % Neighbours=ParEnv % MyPE
               END DO
             END DO
           END DO
         END IF


       ELSE
         MeshPI => Solver % Mesh % ParallelInfo

         ALLOCATE( Matrix % ParallelInfo )
         MatrixPI => Matrix % ParallelInfo

         n = 0
         DO i=1,Mesh % NumberOfBulkElements
           Element => Mesh % Elements(i)
           IF ( .NOT. ASSOCIATED(Element % DGIndexes) ) CYCLE
           n = MAX(n,MAXVAL(Element % DGIndexes))
         END DO

         ALLOCATE( MatrixPI % GlobalDOFs(n) )

         DO i=1,Mesh % NumberOfBulkElements
           Element => Mesh % Elements(i)
           IF ( .NOT. ASSOCIATED(Element % DGIndexes) ) CYCLE
           DO j=1,SIZE(Element % DGIndexes)
             k = Matrix % Perm(Element % DGIndexes(j))
             MatrixPI % GlobalDOFs(k) = 8*(Element % GElementIndex-1)+j
           END DO
         END DO
         ALLOCATE( MatrixPI % Interface(n), MatrixPI % NeighbourList(n) )

         MatrixPI % Interface = .FALSE.
         DO i=1,n
           MtrxN => MatrixPI % NeighbourList(i)
           MtrxN % Neighbours => NULL()
         END DO

         DO i=1,Mesh % NumberOfBulkElements
           Element => Mesh % Elements(i)
           IF ( .NOT. ASSOCIATED(Element % DGIndexes) ) CYCLE
           IF ( ALL(MeshPI % Interface(Element % NodeIndexes)) ) THEN
             DO j=1,Element % Type % NumberOfNodes
                K = Matrix % Perm(Element % DGIndexes(j))
                IF(K==0) CYCLE
                L = Element % Nodeindexes(j)

                MeshN => MeshPI % NeighbourList(L)
                MtrxN => MatrixPI % NeighbourList(K)

                MatrixPI % Interface(k) = .TRUE.

                CALL AllocateVector( MtrxN % Neighbours, &
                      SIZE(MeshN % Neighbours) )
                MtrxN % Neighbours = MeshN % Neighbours
                DO m=1,SIZE(MeshN % Neighbours)
                 IF ( MeshN % Neighbours(m) == Element % PartIndex ) THEN
                   MtrxN % Neighbours(1) = MeshN % Neighbours(m)
                   MtrxN % Neighbours(m) = MeshN % Neighbours(1)
                   EXIT
                 END IF
                END DO
             END DO
           END IF
         END DO
         DO i=1,n
           MtrxN => MatrixPI % NeighbourList(i)
           IF ( .NOT.ASSOCIATED( MtrxN % Neighbours) ) THEN
             CALL AllocateVector(MtrxN % Neighbours,1)
             MtrxN % Neighbours(1) = ParEnv % myPE
           END IF
         END DO
       END IF

       Matrix % ParMatrix => &
          ParInitMatrix( Matrix, Matrix % ParallelInfo )
!if ( parenv % mype == 0 ) print*,'MATRIX INIT TIME: ', realtime()-tt
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelInitMatrix
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelInitSolve( Matrix, x, b, r, Update )
!-------------------------------------------------------------------------------
       REAL(KIND=dp) CONTIG :: x(:), b(:), r(:)
       LOGICAL, OPTIONAL :: Update
       TYPE(Matrix_t), POINTER :: Matrix
!-------------------------------------------------------------------------------
       LOGICAL :: Upd
#ifdef PARALLEL_FOR_REAL
       Upd = .TRUE.
       IF ( PRESENT(Update) ) Upd=Update
       CALL SParInitSolve( Matrix, x, b, r, Matrix % ParallelInfo, Upd )
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelInitSolve
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
    SUBROUTINE ParallelSumVector( Matrix, x, Op )
!-------------------------------------------------------------------------------
       TYPE(Matrix_t) :: Matrix
       INTEGER, OPTIONAL :: op
       REAL(KIND=dp) CONTIG :: x(:)
!-------------------------------------------------------------------------------

       GlobalData => Matrix % ParMatrix
       ParEnv     =  GlobalData % ParEnv
       ParEnv % ActiveComm = Matrix % Comm

       CALL ExchangeSourceVec( Matrix, Matrix % ParMatrix % SplittedMatrix, &
              Matrix % ParallelInfo, x, op )
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelSumVector
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelUpdateSolve( Matrix, x, r )
!-------------------------------------------------------------------------------
       REAL(KIND=dp) CONTIG :: x(:), r(:)
       TYPE(Matrix_t), POINTER :: Matrix
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
       CALL SParUpdateSolve( Matrix, x, r )
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelUpdateSolve
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelMatrixVector( Matrix, x, b, Update, UseMassVals )
!-------------------------------------------------------------------------------
      REAL(KIND=dp) CONTIG :: x(:), b(:)
      LOGICAL, OPTIONAL :: Update, UseMassVals
      TYPE(Matrix_t), POINTER :: Matrix
!-------------------------------------------------------------------------------
      INTEGER :: i,ipar(1)
      REAL(KIND=dp), POINTER CONTIG :: Mx(:), Mr(:), Mb(:), r(:)

      TYPE(SplittedMatrixT), POINTER :: SP
      TYPE(Matrix_t), POINTER :: SavePtrIN
      TYPE(BasicMatrix_t), POINTER :: SavePtrIF(:), SavePtrNB(:)
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
      GlobalData => Matrix % ParMatrix
      GlobalMatrix => Matrix
      ParEnv = GlobalData % ParEnv
      ParEnv % ActiveComm = Matrix % Comm
      IF ( PRESENT( Update ) ) THEN
         IF ( Update ) THEN
            IF ( PRESENT(UseMassVals) ) THEN
               IF ( UseMassVals ) THEN

                  SP => GlobalData % SplittedMatrix
                  ALLOCATE( SavePtrIF( ParEnv % PEs ) )
                  ALLOCATE( SavePtrNB( ParEnv % PEs ) )
                  ALLOCATE( SavePtrIn )
                  DO i=1,ParEnv % PEs
                    IF ( SP % IfMatrix(i) % NumberOfRows /= 0 ) THEN
                       ALLOCATE(SavePtrIF(i) % Values(SIZE(SP % IfMatrix(i) % Values)))
                       SavePtrIF(i) % Values = SP % IfMatrix(i) % Values
                    END IF
                    IF ( SP % NbsIfMatrix(i) % NumberOfRows /= 0 ) THEN
                       ALLOCATE(SavePtrNB(i) % Values(SIZE(SP % NbsIfMatrix(i) % Values)))
                       SavePtrNB(i) % Values = SP % NbsIfMatrix(i) % Values
                    END IF
                  END DO
                  SavePtrIN % Values => SP % InsideMatrix % Values

                  DO i=1,ParEnv % PEs
                    IF ( SP % IfMatrix(i) % NumberOfRows /= 0 ) &
                      SP % IfMatrix(i) % Values = SP % IfMatrix(i) % MassValues

                    IF ( SP % NbsIfMatrix(i) % NumberOfRows /= 0 ) &
                       SP % NbsIfMatrix(i) % Values = SP % NbsIfMatrix(i) % MassValues 
                  END DO
                  SP % InsideMatrix % Values => SP % InsideMatrix % MassValues
               END IF

               Mx => GlobalData % SplittedMatrix % TmpXVec 
               Mr => GlobalData % SplittedMatrix % TmpRVec 
               CALL SParMatrixVector( Mx, Mr, ipar )
               CALL SParUpdateResult( Matrix, x, b, .FALSE. )
            ELSE
               Mx => GlobalData % SplittedMatrix % TmpXVec 
               Mr => GlobalData % SplittedMatrix % TmpRVec 
               CALL SParMatrixVector( Mx, Mr, ipar )
               CALL SParUpdateResult( Matrix, x, b, .FALSE. )
            END IF

            IF ( PRESENT(UseMassVals) ) THEN
               IF ( UseMassVals ) THEN
                  DO i=1,ParEnv % PEs
                    IF ( SP % IfMatrix(i) % NumberOfRows /= 0 ) THEN
                       ALLOCATE(SP % IfMatrix(i) % Values(SIZE(SavePtrIF(i) % Values)))
                       SP % IfMatrix(i) % Values =  SavePtrIF(i) % Values
                    END IF

                    IF ( SP % NbsIfMatrix(i) % NumberOfRows /= 0 ) THEN
                       ALLOCATE(SP % NbsIfMatrix(i) % Values(SIZE(SavePtrNB(i) % Values)))
                       SP % NbsIfMatrix(i) % Values =  SavePtrNB(i) % Values 
                    END IF
                  END DO
                  SP % InsideMatrix % Values => SavePtrIN % Values 
                  DEALLOCATE( SavePtrIF )
                  DEALLOCATE( SavePtrNB )
                  DEALLOCATE( SavePtrIn )
               END IF
            END IF
         ELSE
            IF ( PRESENT(UseMassVals) ) THEN
               IF ( UseMassVals ) THEN
                  SP => Matrix % ParMatrix % SplittedMatrix
                  ALLOCATE( SavePtrIF( ParEnv % PEs ) )
                  ALLOCATE( SavePtrNB( ParEnv % PEs ) )
                  ALLOCATE( SavePtrIn )
                  DO i=1,ParEnv % PEs
                    IF ( SP % IfMatrix(i) % NumberOfRows /= 0 ) THEN
                       ALLOCATE(SavePtrIF(i) % Values(SIZE(SP % IfMatrix(i) % Values)))
                       SavePtrIF(i) % Values = SP % IfMatrix(i) % Values
                    END IF

                    IF ( SP % NbsIfMatrix(i) % NumberOfRows /= 0 ) THEN
                       ALLOCATE(SavePtrNB(i) % Values(SIZE(SP % NbsIfMatrix(i) % Values)))
                       SavePtrNB(i) % Values = SP % NbsIfMatrix(i) % Values
                    END IF
                  END DO
                  SavePtrIN % Values => SP % InsideMatrix % Values

                  DO i=1,ParEnv % PEs
                    IF ( SP % IfMatrix(i) % NumberOfRows /= 0 ) &
                       SP % IfMatrix(i) % Values = SP % IfMatrix(i) % MassValues

                    IF ( SP % NbsIfMatrix(i) % NumberOfRows /= 0 ) &
                       SP % NbsIfMatrix(i) % Values = SP % NbsIfMatrix(i) % MassValues 
                  END DO
                  SP % InsideMatrix % Values => SP % InsideMatrix % MassValues
               END IF
            END IF

            CALL SParMatrixVector( x, b, ipar )

            IF ( PRESENT(UseMassVals) ) THEN
              IF ( UseMassVals ) THEN
                DO i=1,ParEnv % PEs
                  IF ( SP % IfMatrix(i) % NumberOfRows /= 0 ) THEN
                     SP % IfMatrix(i) % Values = SavePtrIF(i) % Values
                     DEALLOCATE(SavePtrIF(i) % Values)
                  END IF

                  IF ( SP % NbsIfMatrix(i) % NumberOfRows /= 0 ) THEN
                     SP % NbsIfMatrix(i) % Values = SavePtrNB(i) % Values 
                     DEALLOCATE(SavePtrNB(i) % Values)
                  END IF
                END DO
                SP % InsideMatrix % Values => SavePtrIN % Values 
                DEALLOCATE( SavePtrIF )
                DEALLOCATE( SavePtrNB )
                DEALLOCATE( SavePtrIN )
              END IF
            END IF
         END IF
      ELSE
         CALL SParMatrixVector( x, b, ipar )
      END IF
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelMatrixVector
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelVector(A, vec)
!-------------------------------------------------------------------------------
      TYPE(Matrix_t), INTENT(in) :: A
      REAL(KIND=dp), INTENT(inout) :: vec(:)
!-------------------------------------------------------------------------------
      INTEGER :: i,j,k
!-------------------------------------------------------------------------------
      j = 0
      DO i=1,A % NumberOfRows
        IF ( A % ParallelInfo % Neighbourlist(i) % &
                   Neighbours(1)==Parenv % Mype ) THEN
          j=j+1
          vec(j) = vec(i)
        END IF
      END DO
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelVector
!-------------------------------------------------------------------------------
    

!-------------------------------------------------------------------------------
    SUBROUTINE ParallelUpdateResult( Matrix, x, r )
!-------------------------------------------------------------------------------
       REAL(KIND=dp) :: x(:), r(:)
       TYPE(Matrix_t), POINTER :: Matrix
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
       CALL SParUpdateResult( Matrix, x, r, .TRUE. )
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelUpdateResult
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelUpdateRHS( Matrix, b )
!-------------------------------------------------------------------------------
       REAL(KIND=dp) :: b(:)
       TYPE(Matrix_t), POINTER :: Matrix
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
       CALL SParUpdateRHS( Matrix, b, Matrix % ParallelInfo )
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelUpdateRHS
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------

    FUNCTION ParallelMatrix( A,x,b,r ) RESULT(M)
!-------------------------------------------------------------------------------
       TYPE(Matrix_t), POINTER :: A, M
       REAL(KIND=dp),  POINTER, OPTIONAL :: x(:),b(:),r(:)
!-------------------------------------------------------------------------------
       M => NULL()
#ifdef PARALLEL_FOR_REAL
       M => A % ParMatrix % SplittedMatrix % InsideMatrix
       IF ( PRESENT(x) ) THEN
          b => M % RHS
          x => A % ParMatrix % SplittedMatrix % TmpXVec
          r => A % ParMatrix % SplittedMatrix % TmpRVec
       END IF
#endif
!-------------------------------------------------------------------------------
    END FUNCTION ParallelMatrix
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    FUNCTION ParallelNorm( n, x ) RESULT(s)
!-------------------------------------------------------------------------------
      INTEGER :: n
      REAL(KIND=dp) :: s
      REAL(KIND=dp) CONTIG :: x(:)
!-------------------------------------------------------------------------------
      s = 0.0d0
#ifdef PARALLEL_FOR_REAL
      s = SParNorm( n, x, 1 )
#endif
!-------------------------------------------------------------------------------
    END FUNCTION ParallelNorm
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    FUNCTION ParallelDOT( n, x, y ) RESULT(s)
!-------------------------------------------------------------------------------
      INTEGER :: n
      REAL(KIND=dp) :: s
      REAL(KIND=dp) CONTIG :: x(:),y(:)
!-------------------------------------------------------------------------------
      s = 0.0d0
#ifdef PARALLEL_FOR_REAL
      s = SParDotProd( n, x, 1, y, 1 )
#endif
!-------------------------------------------------------------------------------
    END FUNCTION ParallelDOT
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelGlobalNumbering(Mesh,OldMesh,NewNodes,IntCnts,IntArray,Reorder)
!-------------------------------------------------------------------------------
       TYPE(Mesh_t) :: Mesh, OldMesh
       INTEGER :: NewNodes,IntCnts(:),IntArray(:),Reorder(:)
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
        CALL SparGlobalNumbering( Mesh,OldMesh,NewNodes,IntCnts,IntArray,Reorder )
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelGlobalNumbering
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
    SUBROUTINE ParallelIter( SourceMatrix, ParallelInfo, DOFs, XVec, &
              RHSVec, Solver, SParMatrixDesc )
!-------------------------------------------------------------------------------
       TYPE (Matrix_t) :: SourceMatrix
       TYPE (ParallelInfo_t) :: ParallelInfo
       INTEGER :: DOFs
       REAL(KIND=dp), DIMENSION(:) :: XVec, RHSVec
       TYPE (Solver_t) :: Solver
       TYPE (SParIterSolverGlobalD_t), POINTER :: SParMatrixDesc
                                                                                                                               
#ifdef PARALLEL_FOR_REAL
       CALL SParIterSolver( SourceMatrix, ParallelInfo, XVec, &
                 RHSVec, Solver, SParMatrixDesc )
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelIter
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelActive( L )
!-------------------------------------------------------------------------------
      LOGICAL :: L
#ifdef PARALLEL_FOR_REAL
       IF ( ParEnv % PEs > 1) THEN
         CALL SParIterBarrier
         CALL SParIterActive(L)
       END IF
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelActive
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelAllReduceAnd( L )
!-------------------------------------------------------------------------------
      LOGICAL :: L
#ifdef PARALLEL_FOR_REAL
       IF ( ParEnv % PEs > 1) CALL SParIterAllReduceAnd(L)
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelAllReduceAnd
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    FUNCTION ParallelReduction(R,oper_arg) RESULT(rsum)
!-------------------------------------------------------------------------------
      REAL(KIND=dp) :: R, rsum
      INTEGER, OPTIONAL :: oper_arg
!-------------------------------------------------------------------------------
      INTEGER :: oper
!-------------------------------------------------------------------------------
      rsum = r
#ifdef PARALLEL_FOR_REAL
      IF ( ParEnv % PEs>1) THEN
        oper = 0
        IF (PRESENT(oper_arg)) THEN
          oper=oper_arg
        ELSE
          oper = 0
        END IF
        CALL SparActiveSUM(rsum,oper)
      END IF
#endif
!-------------------------------------------------------------------------------
    END FUNCTION ParallelReduction
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelBarrier
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
       IF ( ParEnv % PEs > 1) CALL SParIterBarrier
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelBarrier
!-------------------------------------------------------------------------------


!-------------------------------------------------------------------------------
    SUBROUTINE ParallelActiveBarrier
!-------------------------------------------------------------------------------
#ifdef PARALLEL_FOR_REAL
       IF ( ParEnv % PEs > 1 ) CALL SParIterActiveBarrier
#endif
!-------------------------------------------------------------------------------
    END SUBROUTINE ParallelActiveBarrier
!-------------------------------------------------------------------------------

END MODULE ParallelUtils

!> \}
